/*
 * BST.cpp
 *
 *  Created on: Oct 20, 2020
 *      Author: Galen Nare
 */

#include "BST.hpp"


BST::BST() {
	root = nullptr;
}
BST::BST(string s) {
	root = new TNode(s);
}

void BST::printTreeIO() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing In Order:" <<endl;
		printTreeIO(root);
	}
}


void BST::printTreePre() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing PreOrder:" <<endl;
		printTreePre(root);
	}
}


void BST::printTreePost() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing PostOrder:" <<endl;
		printTreePost(root);
	}
}


void BST::clearTree() {
	if (root == nullptr) {
		cout << "Tree already empty" << endl;
	}
	else {
		cout << endl << "Clearing Tree:" << endl;
		clearTree(root);
		root = nullptr;
	}
}
void BST::clearTree(TNode *tmp) {
	if (tmp == nullptr) {
		return;
	}
	else {
		clearTree(tmp->left);
		clearTree(tmp->right);
		tmp->printNode();
		delete(tmp);
	}
}

TNode* BST::find(string data) {
	return find(data, root);
}

TNode* BST::find(string data, TNode* start) {
	if (start == nullptr) {
		return nullptr;
	}

	if (start->data->phrase == data) {
		return start;
	} else if (data.compare(start->data->phrase) < 0) {
		return find(data, start->left);
	} else {
		return find(data, start->right);
	}
}

bool BST::insert(string data) {
	if (root == nullptr) {
		TNode* node = new TNode(data);
		root = node;
		setHeight(node);
		return true;
	}

	return insert(data, root);
}

bool BST::insert(string data, TNode* start) {
	TNode* node = new TNode(data);
	
	if (start->data->phrase == data) {
		return false; // Don't insert duplicates
	} else if (data.compare(start->data->phrase) < 0) {
		if (start->left == nullptr) {
			start->left = node;
			node->parent = start;
			setHeight(node);
			return true;
		} else {
			return insert(data, start->left);
		}
	} else {
		if (start->right == nullptr) {
			start->right = node;
			node->parent = start;
			setHeight(node);
			return true;
		} else {
			return insert(data, start->right);
		}
	}
}

int BST::setHeight(TNode* node) {
	return node->height = 1 + ((node->parent != nullptr) ? setHeight(node->parent) : 0);
}

void BST::printTreeIO(TNode* node) {
	if (node == nullptr) return;

	if (node->left != nullptr) {
		printTreeIO(node->left);
	}

	node->printNode();

	if (node->right != nullptr) {
		printTreeIO(node->right);
	}
}

void BST::printTreePost(TNode* node) {
	if (node == nullptr) return;

	if (node->left != nullptr) {
		printTreePost(node->left);
	}

	if (node->right != nullptr) {
		printTreePost(node->right);
	}

	node->printNode();
}

void BST::printTreePre(TNode* node) {
	if (node == nullptr) return;

	node->printNode();
	if (node->left != nullptr) {
		printTreePre(node->left);
	}

	if (node->right != nullptr) {
		printTreePre(node->right);
	}
}

TNode* BST::remove(string data) {
	TNode* node = find(data);
	
	if (node->left == nullptr && node->right == nullptr) {
		return removeNoKids(node);
	} else if (node->left != nullptr && node->right == nullptr) {
		return removeOneKid(node, true);
	} else if (node->right != nullptr && node->left == nullptr) {
		return removeOneKid(node, false);
	} else {
		TNode* child = node->left;
		while(child->right != nullptr) {
			child = child->right;
		}
		if (child != node->left) {
			child->left = node->left;
		} else {
			child->left = node->left->left;
		}
		child->right = node->right;

		child->parent->right = nullptr;

		child->parent = node->parent;

		if (child->parent == nullptr) {
			root = child;
		}

		if (node->parent != nullptr && node->parent->left == node) {
			node->parent->left = child;
		} else if (node->parent != nullptr && node->parent->right == node) {
			node->parent->right = child;
		}

		node->left = nullptr;
		node->right = nullptr;
		node->parent = nullptr;
		return node;
	}
}

TNode* BST::removeNoKids(TNode* node) {
	TNode* parent = (node->parent == nullptr) ? nullptr : node->parent;
	
	if (parent != nullptr && parent->left == node) {
		parent->left = nullptr;
	}
	
	if (parent != nullptr && parent->right == node) {
		parent->right = nullptr;
	}

	node->parent = nullptr;
	return node;
}

TNode* BST::removeOneKid(TNode* node, bool leftFlag) {
	TNode* parent = (node->parent == nullptr) ? nullptr : node->parent;

	if (parent != nullptr && parent->left == node) {
		TNode* child = (leftFlag) ? node->left : node->right;
		parent->left = child;
		child->parent = parent;
	} else if (parent != nullptr && parent->right == node) {
		TNode* child = (leftFlag) ? node->left : node->right;
		parent->right = child;
		child->parent = parent;
	}

	node->parent = nullptr;
	node->left = nullptr;
	node->right = nullptr;
	return node;
}
